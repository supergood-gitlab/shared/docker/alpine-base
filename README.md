# Base Image

[![Docker Stars](https://img.shields.io/docker/stars/mattrybin/alpine-base.svg?style=flat-square)](https://hub.docker.com/r/mattrybin/alpine-bash/)
[![Docker Pulls](https://img.shields.io/docker/pulls/mattrybin/alpine-base.svg?style=flat-square)](https://hub.docker.com/r/mattrybin/alpine-bash/)


Base Docker image
=================

This image is based on Alpine Linux image, which is only a 5MB image, and
contains [Bash](https://www.gnu.org/software/bash/) (Bourne Again SHell) with
some useful tools (e.g., complete implementations of grep, sed, awk, bc, head,
tail, and etc).

Download size of this image is only:

[![](https://images.microbadger.com/badges/image/mattrybin/alpine-base.svg)](http://microbadger.com/images/mattrybin/alpine-base "Get your own image badge on microbadger.com")


Usage Example
-------------

```bash
$ docker run --rm mattrybin/alpine-base bash -c 'echo "Hello World"'
```

Once you have run this command you will get printed 'Hello World' from Bash!